import React from 'react'
import { View, Text } from 'react-native'
import { TextInput, Button } from 'react-native-paper'


const RegisterScreen = () => {
    return (
        <View style={{flex: 1, padding: 20}}>
            <Text>Nimi: </Text>
            <TextInput label="Nimi" mode="outlined" />
            <Text>Sähköposti: </Text>
            <TextInput label="esim@esim.com" mode="outlined" />
            <Text>Salasana: </Text>
            <TextInput label="Salasana" mode="outlined" secureTextEntry={true} />
            <Text>Salasana uudestaan: </Text>
            <TextInput label="Salasana uudestaan" mode="outlined" secureTextEntry={true} />

            <Button title="Rekisteröidy" icon="account" mode="contained" style={{marginTop: 10}} onPress={() => console.log('rekisteröidy nappia painettu')}>
                <Text>Rekisteröidy</Text>
            </Button>
        </View>
    )
}

export default RegisterScreen