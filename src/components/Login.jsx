import React, { useState } from 'react'
import { TextInput, Button, Text } from 'react-native-paper'
import { View } from 'react-native'

const Login = ( {navigation} ) => {
    const [tunnus, setTunnus] = useState('')
    return (
        <View style={{flex: 1, padding: 20}}>
            <Text>Tunnus: </Text>
            <TextInput label="esim@esim.com" mode="outlined" onChangeText={tunnus => setTunnus(tunnus)} right={<TextInput.Icon name="account"/>}/>
            <Text>Salasana: </Text>
            <TextInput label="Salasana" mode="outlined" secureTextEntry={true} right={<TextInput.Icon name="eye" />}/>
            <Button title="Sisään" mode="contained" onPress={() => navigation.navigate({name: 'Main Screen', params: {tunnus}})} style={{marginTop: 10}} > 
                Sisään
            </Button>
            <Button title="Register" icon="account-plus" onPress={() => navigation.navigate({name: 'Register'})} color="black" >
                <Text style={{fontSize: 11}}>Rekisteröidy</Text>
            </Button>
        </View>
    )
}

export default Login