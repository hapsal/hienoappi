import React from 'react'
import { View, Text, FlatList, SafeAreaView } from 'react-native'
import { Button, Divider } from 'react-native-paper'


const MainScreen = ( {route, navigation} ) => {
    const testiData = [
        {
            id: 1,
            title: "Esimerkki tapahtuma 1"
        },
        {
            id: 2,
            title: "Esimerkki tapahtuma 2"
        },
        {
            id: 3,
            title: "Esimerkki tapahtuma 3"
        },
        {
            id: 4,
            title: "Esimerkki tapahtuma 4"
        },
        {
            id: 5,
            title: "Esimerkki tapahtuma 5"
        },
        {
            id: 6,
            title: "Esimerkki tapahtuma 6"
        },
        {
            id: 7,
            title: "Esimerkki tapahtuma 7"
        },
        {
            id: 8,
            title: "Esimerkki tapahtuma 8"
        },
        {
            id: 9,
            title: "Esimerkki tapahtuma 9"
        },
    ]
    
    
                    

    const { tunnus } = route.params
    return (
        <View>
            <Text>Tunnus: {JSON.parse(JSON.stringify(tunnus))}</Text>
            <SafeAreaView>
                <FlatList data={testiData} renderItem={({item}) => 
                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 100, margin: 5, backgroundColor: '#00BCD4'}}>
                        <Text style={{padding: 10, justifyContent: 'center'}} onPress={() => {alert('Tapahtumaa painettu')}}>{item.title}</Text>
                    </View>
                    } 
                numColumns={3} />
            </SafeAreaView>
            <Button title="Lisää uusi tapahtuma" mode="contained" onPress={() => navigation.navigate({name: 'New Event'})}> 
                Lisää
            </Button>
        </View>
        
    )
}

export default MainScreen