import React from 'react'
import { Appbar } from 'react-native-paper';


const CustomNavigationBar = ( { navigation, previous } ) => {
    return (
        <Appbar.Header>
            { previous ? <Appbar.BackAction onPress={navigation.goBack} /> : null }
            <Appbar.Content title="Hieno äppi" />
        </Appbar.Header>
    )
}

export default CustomNavigationBar