import React from 'react'
import { TextInput, Button } from 'react-native-paper'
import { View } from 'react-native'

const NewEvent = () => {
    return (
        <View>
            <TextInput label="Laji" mode="outlined" />
            <TextInput label="Ihmisten lukumäärä" mode="outlined" />
            <TextInput label="Päivämäärä ja kello" mode="outlined" />
            <TextInput label="Tapaamispaikka tms" mode="outlined" />

            <Button title="Lisää uusi tapahtuma" mode="contained" onPress={() => console.log('lisää nappia painettu')}> 
                Lisää
            </Button>
        </View>
    )
}

export default NewEvent