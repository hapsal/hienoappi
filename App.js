import 'react-native-gesture-handler'
import React from 'react';
import { Provider as PaperProvider } from 'react-native-paper'
import Login from './src/components/Login'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'  
import MainScreen from './src/components/MainScreen';
import RegisterScreen from './src/components/RegisterScreen'
import CustomNavigationBar from './src/components/CustomNavigationBar'
import NewEvent from './src/components/NewEvent';

const Stack = createStackNavigator()

const App = () => {
  return (
    <PaperProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" screenOptions={{header: (props) => <CustomNavigationBar {...props} />}}>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Main Screen" component={MainScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          <Stack.Screen name="New Event" component={NewEvent} />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  )
}

export default App